def calcDuration(data):
    if(data):
        if(data['Status']=='Baru'):
            return 0
        elif(data['Status']=='Verifikasi'):
            return (data['Tgl Verifikasi'] - data['Tgl Lapor'])
        elif(data['Status']=='Dalam Proses'):
            return (data['Tgl Dlm Proses'] - data['Tgl Lapor'])
        elif(data['Status']=='Selesai' or data['Status']=='Selesai Bersyarat'):
            return (data['Tgl Dlm Proses'] - data['Tgl Lapor'])
        else:
            return False
    else:
        return 0